const fs = require('fs');
const path = require('path');

const problem2 = () => {

    const readFile = (fileName, filePath) => {
        return new Promise((resolve, reject) => {
            fs.readFile(filePath, 'utf-8', (err, data) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(data);
                }
            })
        })
    }

    const writeFile = (fileName, filePath, dataToWrite) => {
        return new Promise((resolve, reject) => {
            fs.writeFile(filePath, dataToWrite, (err) => {
                if (err) {
                    reject(err);
                } else {
                    resolve({ fileName: fileName, filePath: filePath });
                }
            })
        })
    }

    const appendFileNames = (fileNamesFilePath, fileNameToAdd) => {
        return new Promise((resolve, reject) => {
            fs.appendFile(fileNamesFilePath, `${fileNameToAdd}\n`, (err) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(fileNameToAdd);
                }
            })
        })
    }

    const deleteFile = (fileName, filePath) => {
        return new Promise((resolve,reject) => {
            fs.unlink(filePath, (err) => {
                if(err) {
                    reject(err);
                } else {
                    console.log(`${fileName} deleted`);
                    resolve(fileName);
                }
            })
        })
    }

    const lipsumDataFile = 'lipsum.txt';
    const lipsumDataFilePath = path.join(__dirname, lipsumDataFile);

    const fileNamesFile = 'filenames.txt';
    const fileNamesFilePath = path.join(__dirname, fileNamesFile);

    const upperCaseDataFile = 'upperCasedData.txt';
    const upperCaseDataFilePath = path.join(__dirname, upperCaseDataFile);

    const lowerCasedDataFile = 'lowerCasedSentences.txt'
    const lowerCasedDataFilePath = path.join(__dirname, lowerCasedDataFile);


    //reading lipsum data file and getting the data.
    readFile(lipsumDataFile, lipsumDataFilePath)
        .then((lipsumData) => {
            console.log(`lipsum.txt file data read successfully\n`);

            const upperCaseData = lipsumData.toUpperCase();
            return writeFile(upperCaseDataFile, upperCaseDataFilePath, upperCaseData);
        })
        .then((upperCaseFileDetailsObj) => {
            console.log(`uppercase data written successfully to ${upperCaseDataFile}`);

            return appendFileNames(fileNamesFilePath, upperCaseFileDetailsObj.fileName);
        })
        .then((upperCaseFileName) => {
            console.log(`${upperCaseFileName} added to ${fileNamesFile} successfully`);

            return readFile(upperCaseDataFile, upperCaseDataFile);
        })
        .then((upperCaseData) => {
            console.log(`uppercase data read successfully from ${upperCaseDataFile}`);

            const lowerCasedSentences = upperCaseData.toLowerCase().split('.').map((sentence) => {
                return sentence.trim();
            })
                .filter((sentence) => {
                    return sentence !== "";
                })
                .join('\n');

            return writeFile(lowerCasedDataFile, lowerCasedDataFilePath, lowerCasedSentences);
        })
        .then((lowerCaseFileDetailsObj) => {
            console.log(`lowercased data written successfully to ${lowerCaseFileDetailsObj.fileName}`);

            return appendFileNames(fileNamesFilePath, lowerCaseFileDetailsObj.fileName);
        })
        .then((lowerCaseFileName) => {
            console.log(`${lowerCaseFileName} added successfully to ${fileNamesFile}`);

            return readFile(upperCaseDataFile, upperCaseDataFilePath);
        })
        .then((upperCaseData) => {
            console.log(`${upperCaseDataFile} data read successfully`);

            const upperCaseSorted = upperCaseData.split('.').map((sentence) => {
                return sentence.trim();
            })
            .filter((sentence) => {
                return sentence !== "";
            })
            .sort()
            .join('\n')

            const upperCaseSortedFile = 'upperCaseSortedSentences.txt'
            const upperCaseSortedFilePath = path.join(__dirname,upperCaseSortedFile)

            return writeFile(upperCaseSortedFile, upperCaseSortedFilePath, upperCaseSorted);
        })
        .then((upperCaseSortedFileDetailsObj) => {
            console.log(`uppercase sentences sorted and written to ${upperCaseSortedFileDetailsObj.fileName}`);

            return appendFileNames(fileNamesFile, upperCaseSortedFileDetailsObj.fileName);
        }).then((upperCaseSortedFile) => {
            console.log(`${upperCaseSortedFile} added successfully to ${fileNamesFile}`);

            return readFile(lowerCasedDataFile, lowerCasedDataFilePath);
        })
        .then((lowerCaseData) => {
            console.log(`lowerCase data read successfully from ${lowerCasedDataFile}`);

            const lowerCaseSorted = lowerCaseData.split('\n').sort().join('\n');
            const lowerCaseSortedFile = 'lowerCaseSortedSentences.txt';
            const lowerCaseSortedFilePath = path.join(__dirname, lowerCaseSortedFile);

            return writeFile(lowerCaseSortedFile, lowerCaseSortedFilePath, lowerCaseSorted);
        })
        .then((lowerCaseSortedFileDetailsObj) => {
            console.log(`lowerCase sorted sentences written successfully to ${lowerCaseSortedFileDetailsObj.fileName}`);

            return appendFileNames(fileNamesFile, lowerCaseSortedFileDetailsObj.fileName);
        })
        .then((lowerCaseSortedFile) => {
            console.log(`${lowerCaseSortedFile} added successfully to ${fileNamesFile}`);

            return readFile(fileNamesFile, fileNamesFilePath);
        })
        .then((newFileNames) => {
            console.log(`newly added fileNames successfully read from ${fileNamesFile}`);

            const newFileNamesArray = newFileNames.split('\n').filter((fileName) => { return fileName !== ""});
            const deletionPromises = newFileNamesArray.map((file) => {
                const filePath = path.join(__dirname, file);

                return deleteFile(file,filePath);
            })

            return Promise.all(deletionPromises);
        })
        .then(() => {
            console.log("all files deleted");

        })
        .catch((err) => {
            console.error(`Error occurred: `, err);
        })

}

module.exports = problem2;