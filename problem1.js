const fs = require('fs');
const path = require('path');

const problem1 = () => {

    const makeDirectory = () => {
        return new Promise((resolve, reject) => {
            const dirName = "jsonFiles";
            const dirPath = path.join(__dirname, dirName);
        
            fs.mkdir(dirPath, (err) => {
                if (err) {
                    reject(err);
                } else {
                    resolve({"dirName":dirName, "dirPath":dirPath});
                }
            })
        })
    }

    //generates a array of jsonfiles and creates them in a given directory.
    //returns a promise that all the file creations will be done.
    const jsonFilesCreation = (dirPath) => {
        const minNumberOfFiles = 5;
        const maxNumberOfFiles = 15;
        const numberOfFiles = Math.floor(Math.random() * (maxNumberOfFiles - minNumberOfFiles + 1) + minNumberOfFiles)
        const jsonFiles = [...Array(numberOfFiles).keys()].map((index) => {
            let fileName = `file${index + 1}.json`;
            return fileName;
        })

        //returns a new promise that a new file will be created.
        const createFile = (currFileName, currFilePath) => {
            return new Promise((resolve) => {
                let fileStatusObj = { status: "", fileName: currFileName, filePath: currFilePath };
                const fileContent = JSON.stringify({ fileName: currFileName, filePath: currFilePath}, null, 2);
                fs.writeFile(currFilePath, fileContent, (err) => {
                    if (err) {
                        console.error(`${currFileName} can't be created`, err);
                        fileStatusObj.status = err;
                    } else {
                        console.log(`${currFileName} created successfully`);
                        fileStatusObj.status = 'created';
                    }
                    resolve(fileStatusObj);
                })
            })

        }

        //returns an array of promises for every file in jsonFiles
        const jsonFilePromises = jsonFiles.map((fileName) => {
            let currFilePath = path.join(dirPath, fileName)
            return createFile(fileName, currFilePath);
        })

        return Promise.all(jsonFilePromises);
    }


    const deleteCreatedJsonFiles = (jsonFilesStatusAndPaths) => {

        //returns a new promise that a given file will be deleted
        const deleteFile = (currFileName, currFilePath) => {
            return new Promise((resolve) => {
                let fileDeletionStatusObj = {status:"",fileName: currFileName, filePath: currFilePath};
                fs.unlink(currFilePath, (err) => {
                    if (err) {
                        console.error(`can't delete ${currFileName}`, err);
                        fileDeletionStatusObj.status = err;
                    } else {
                        console.log(`${currFileName} deleted succesfully`);
                        fileDeletionStatusObj.status = "deleted";
                    }
                    resolve(fileDeletionStatusObj);
                })
            })
        }

        const fileDeletionPromises = jsonFilesStatusAndPaths.filter((file) => {
            //filters the successfully created files only.
            return file.status === 'created' && !(file.status instanceof Error);
        })
        .map((file) => {
            //returns a promise for deleting a file 
            return deleteFile(file.fileName, file.filePath);
        })

        return Promise.all(fileDeletionPromises);
    }

    // chaining the creation and deletion of json file to the makeDirectory promise
    makeDirectory()
    .then((directoryDetailsObj) => {
        console.log(`directory ${directoryDetailsObj['dirName']} created successfully`);
        console.log("\njson file creation started : \n");

        //A promise is returned by jsonFilesCreation that confirms all files are undergone creation process
        return jsonFilesCreation(directoryDetailsObj['dirPath']);
    })
    .then((jsonFilesStatusAndPaths) => {

        console.log("\njson file deletion started :\n");
        return deleteCreatedJsonFiles(jsonFilesStatusAndPaths);
    })
    .then((fileDeletionStatusAndPaths) => {
        const deletionErrorFiles = fileDeletionStatusAndPaths.filter((file) => {
            return file.status instanceof Error;
        })
        .map((file) => {
            return file.fileName;
        })

        if(deletionErrorFiles.length>0){
            console.log('Error occurred while deleting these files', deletionErrorFiles);
        }
        console.log("creation and deletion completed");
    })
    .catch((err) => {
        console.error(err);
    });


}


module.exports = problem1;